### Tools Needed ###
python3 with jupiter, ROOT, matplotlib, numpy, pandas,


### PROTEZIONE CIVILE ####
### How to produce histogram needed as input for comparisons in the waves paper:
run this notebook ISTAT/Notebook-PerPaper-ProtezioneCivile.ipynb


### ISTAT ###
### 1) How to convert istat zip file in csv file used as input in the next step
cat comuni_giornaliero_28febbraio22.csv | sed -e 's%n.d.%%g' > comuni_giornaliero_28febbraio22_fixed.csv
(cfr. this command has a bug. Once run it, i usually open the csv file and i fix the "Ar000*"--->"Arnd,000*" and "g000*"->"gnd,000*". (two comuni of valle d'aosta).)

### 2) How to produce tree needed as input for bkg subtraciton plots for waves paper:
run this notebook: ISTAT/MakeTreeFromISTAT.ipynb

### How to make Waves plots for paper (need as input also the protezione civile tree produced above):
run these notebooks: Notebook-PerPaper-Subtraction-*Wave.ipynb

### AREU ###
### 1)How to convert AREU 7z files in tab files used as input of next step
brew install p7zip
7za x SAPEINZA_PZ_INFO-2019new.7z 
cat SAPEINZA_PZ_INFO-2019new.tab | sed -e 's%NaN%%g' > SAPEINZA_PZ_INFO-2019new_fixed.tab
cat SAPEINZA_PZ_INFO-2019new_fixed.tab | sed -e 's%nan%%g' > SAPEINZA_PZ_INFO-2019new_fixed2.tab


### 2) How to produce areu ntuples, the input to bdt optimization:
run this notebook--> AREU/MakeTreeFromAREU.ipynb


### Keep in mind this legend for the output ntples

#------------- DS_SEVERITY_LEVEL_NUM ----

'BIANCO'=0

'GIALLO'=3

'ROSSO'=4

'ROSSO+'=5

'VERDE'=1

'VERDE>GIALLO'=2

#------------- AC_PAT_SEX_NUM -------------

'F'=0

'M'=1

'N'=2

#------------- COD_PAZ_NUM ----------------

'BIANCO'=0

'GIALLO'=2

'NERO'=4

'ROSSO'=3

'VERDE'=1

'_NULL (DATO NON INSERITO)'=999


### 3) run AREU BDT on 5 variables and fill a tree in the same format of the istat one in order to have the DNN distribution for each day corresponding to istat DDAY.
run AREU/MakeTreeFromAREU-5variables.ipynb